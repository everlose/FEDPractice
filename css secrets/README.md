## css代码复用的一点小小的考虑

### 缩减代码

在软件开发过程中，保持代码的简洁和可维护性是最大的挑战，对于 CSS 来说，同样如此。实际上，可维护代码的一个重要特性就是要缩简需求变化时所需修改的代码量。假设放大一个按钮需要对十处代码做出修改，那么你就有可能遗漏其中的一些细节，如果这些代码本来就不是你写的，那么就更有可能发生这种疏漏。

此外，这并不只是应对未来的需求变化。可扩展的 CSS 在完成首次编写后，只需要用少量代码创建适当的变量，那么进行重写和覆盖时所需的代码量就会很少。下面让我们来看一个例子。

请先看一下下面的 CSS，它被用来美化下图所示的按钮：

```css
.btn {
  padding: 6px 16px;
  border: 1px solid #446d88;
  background: #58a linear-gradient(#77a0bb, #58a);
  border-radius: 4px;
  box-shadow: 0 1px 5px gray;
  color: white;
  text-shadow: 0 -1px 1px #335166;
  font-size: 20px;
  line-height: 30px;
  text-decoration: none;
}
```

这段代码在可维护性上有几点问题，假若需求里需要一个更大号更显眼的按钮，首先我们能看到的是必定会修改字体大小。 但是与此同时我们定死了行高，以至于每次需要手动去计算行高数值填入。 当属性值相互关联时，应该在代码中体现它们的关联性。 对于上述的代码，行间距是行高的 150%，所以，使用下面的代码将更具可维护性：

```css
font-size: 20px;
line-height: 150%;
```

既然我们都写成了这样，为什么还要给字体大小指定一个绝对数值？不如让按钮设置的比父元素字体大小大一些，这样更好的方式是使用百分比或者类似 em 的单位：

```css
font-size: 125%; / 假设父级字体为 16px /
line-height: 150;
```

这是因为其他的特效还是和之前一样定死像素大小，仍然不具有伸缩性。 只需要使用类似 em 的单位，我们就可以将其他特效变成可伸缩的，最终所有的属性值都关联到了字体大小上。 至此，我们只需修改字体大小就能控制整个按钮的大小了。

```css
.btn {
  padding: .3em .8em;
  border: 1px solid #446d88;
  background: #58a linear-gradient(#77a0bb, #58a); 
  border-radius: .2em;
  box-shadow: 0 .05em .25em gray;
  color: white;
  text-shadow: 0 -.05em .05em #335166;
  font-size: 125%;
  line-height: 150%;
  text-decoration: none;
}
```

值得一题的是，在这里，我们想要字体大小与父级字体相关联，所以使用了 em。在某些情况下，你想要让字体和根节点的字体相关联（比如 节点的字体），但是这时使用 em 就会让计算变得非常复杂。此时，最好使用 rem 单位。虽然“相关性”在 CSS 中很重要，但你关联前需要考虑下什么元素需要被关联在一起。

现在，按钮看起来很像是原始版本的放大版: 

通常我们所需要修改的不只是按钮的尺寸。对配色的修改也是一个很重要的方面。 比如，如果我们想要创建一个红色的“取消”按钮，或者是绿色的 “OK” 按钮又该如何做呢？ 一般来说，我们至少需要重写四条样式（border-color，background，box-shadow，text-shadow）。 不用多说你也会理解，重新计算主体颜色（#58a）的亮度、理清所需颜色的亮度，将是一份非常麻烦的事情。 此外，如果我们将按钮添加到了非白色背景的页面中，又该如何修改呢？实际上，上述按钮的灰色阴影只在白色背景下才会效果明显。

一个简单的修改方式是，对主体颜色的亮度叠加半透明的黑白色：

```css
.btn {
  padding: .3em .8em;
  border: 1px solid rgba(0,0,0,.1);
  background: #58a linear-gradient(rgba(0,0%,100%,.2),transparent);
  border-radius: .2em;
  box-shadow: 0 .05em .25em rgba(0,0,0,.5);
  color: white;
  text-shadow: 0 -.05em .05em rgba(0,0,0,.5);
  font-size: 125%;
  line-height: 150%;
  text-decoration: none;
}
```

接下来我们就可以重写颜色了

### 代码的复用性

假如有一个特殊的场景，需要一个特殊的按钮，或是更长更扁，或是颜色变换，或者是圆角阴影的变化。 参照bootstrap做法，我试着改写我的场景。

```css
.btn {
  padding: .3em .8em;
  border: 1px solid rgba(0,0,0,.1);
  background: #58a linear-gradient(rgba(255,255,255,.2),transparent);
  border-radius: .2em;
  box-shadow: 0 .05em .25em rgba(0,0,0,.5);
  color: white;
  text-shadow: 0 -.05em .05em rgba(0,0,0,.5);
  font-size: 125%;
  line-height: 150%;
  text-decoration: none;
}

.special > .btn{
  padding: .3em 1.2em;
  background-color: #0f0;
  color: #f00;
  text-decoration: underline;

}
```

这样的做法如果对于页面级的，不再去复用代码的，已经够用了。 缺点自然是CSS限制了组件、布局的结构。只能套，很难改，限制死，不自由。
可以试试如下的方案去复用代码。

```css
.btn {
  padding: .3em .8em;
  border: 1px solid rgba(0,0,0,.1);
  background: #58a linear-gradient(rgba(255,255,255,.2),transparent);
  border-radius: .2em;
  box-shadow: 0 .05em .25em rgba(0,0,0,.5);
  color: white;
  text-shadow: 0 -.05em .05em rgba(0,0,0,.5);
  font-size: 125%;
  line-height: 150%;
  text-decoration: none;
}
.btn-color-red {
  color: #f00; 
}
.btn-bg-green {
  background-color: #0f0;
}
.btn-flat {
  padding: .3em 1.2em;
}
.btn-tdl { 
  text-decoration: underline; 
}
```
这个方法优点是灵活度的确是最大了，按钮简直可以千变万化，但是代价也高。 但是css文件必定变长很多，可能我们写了50行的颜色定义的css，最后却只用了一处，冗余代码多。
最后，其实完全不会有那么多要复用的东西，考虑这些颇有几分庸人自扰的想法。