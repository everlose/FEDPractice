## 表单组件文档
表单所需要使用的一些组件代码

### 目录
$(selector).datetimepicker(option) 日历控件
uploadify图片上传


### datimepicker
依赖stfile/jquery.datetimepicker.css
依赖stfile/jquery.datetimepicker.js
```
$(selector).datetimepicker({
    lang : 'ch', // 语言
    timepicker: true,
    format: 'Y/m/d H:i',
    closeOnDateSelect: true, // 在选择日期后 是否直接关闭
    yearStart: 1970, //最少可从1970年开始选年份
    yearEnd: 2100, //最多年份选到2100
    onShow: function (ct) {
        // 时间控件 显示时的回调
    } ,
    onSelectDate:function(date) {
        // 日期选择时的回调         
    }
});
```
#### 参数解析
format: 时间和日期的格式， 默认为Y/m/d H:i。 相似的还有formatTime时间格式，formatDate日期格式。 
lang： 语言，ch为中文。 
todayBtn： 今天所在的日期高亮显示。 
onShow（）：控件显示的时候触发的事件。 
timepicker： 时间选择，为true则显示，为false则不显示此模块。 
datepicker： 日期选择，为true则显示，为false则不显示此模块。 
closeOnDateSelect： 选择完日期或时间后隐藏控件。


